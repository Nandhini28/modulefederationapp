import React from 'react';
export default function Widget(data) {
  const result = Object.values(data).map((eachItem) => eachItem);
  return (
    <div
      style={{
        borderRadius: '4px',
        padding: '2em',
        backgroundColor: 'purple',
        color: 'white',
      }}
    >
      <h2>Check Addition Number is Odd or Even</h2>
      <p style={{fontWeight: "bold"}}>
        Addition Number is {result % 2 == 0 ? "Even" : "Odd"}
      </p>
    </div>
  );
}
