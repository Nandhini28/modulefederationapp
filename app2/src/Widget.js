// import React from 'react';
// export default function Widget(data) {  
//   const val = Object.values(data).map((eachItem) => eachItem);
//   return (
//     <div>
//       <h2>Addition Number {val} <br/> Multiply by 2</h2>
//       <p>
//       {val*2}
//       </p>
//     </div>
//   );
// }

import React from 'react';
function loadComponent(scope, module) {
  return async () => {
    await __webpack_init_sharing__('default');
    const container = window[scope]; 
    await container.init(__webpack_share_scopes__.default);
    const factory = await window[scope].get(module);
    const Module = factory();
    return Module;
  };
}
export const useFederatedComponent = (remoteUrl, scope, module, data) => {
  const key = `${remoteUrl}-${scope}-${module}`;
  const [Component, setComponent] = React.useState(data);
  const { ready, errorLoading } = useDynamicScript(remoteUrl);
  React.useEffect(() => {
    if (Component) setComponent(null);
    // Only recalculate when key changes
  }, [key]);

  React.useEffect(() => {
    if (ready && !Component) {
      const Comp = React.lazy(loadComponent(scope, module));
      componentCache.set(key, Comp);
      setComponent(Comp);
    }
  }, [Component, ready, key]);

  return { errorLoading, Component };
};

export default function Widget(data) {
  const val = Object.values(data).map((eachItem) => eachItem);
  const [{ module, scope, url }, setSystem] = React.useState({});
  function findOddEven() {
    setSystem({
      url: 'http://localhost:3003/remoteEntry.js',
      scope: 'app3',
      module: './Widget',
    });
  }
  const { Component: FederatedComponent, errorLoading } = useFederatedComponent(url, scope, module);
  return (
    <div>
      <h2 style={{textAlign: "center"}}>Multiply by 2</h2>
      <div className="multiby">
        <p style={{textAlign: "center"}}>Given Number - {val}</p>
        <p style={{textAlign: "center"}}>Multiple by 2 is {val*2}</p>
        <p style={{textAlign: "center"}}>
          <button onClick={findOddEven}>Check the number odd or even</button>
        </p>
      </div>
      <div style={{ marginTop: '2em' }}>
        <React.Suspense fallback="Loading System">
          {errorLoading
            ? `Error loading module "${module}"`
            : FederatedComponent && <FederatedComponent data={val} />}
        </React.Suspense>
      </div>
    </div>
  );
}
const urlCache = new Set();
const useDynamicScript = url => {
  const [ready, setReady] = React.useState(false);
  const [errorLoading, setErrorLoading] = React.useState(false);
  React.useEffect(() => {
    if (!url) return;
    if (urlCache.has(url)) {
      setReady(true);
      setErrorLoading(false);
      return;
    }
    setReady(false);
    setErrorLoading(false);
    const element = document.createElement('script');
    element.src = url;
    element.type = 'text/javascript';
    element.async = true;
    element.onload = () => {
      urlCache.add(url);
      setReady(true);
    };
    element.onerror = () => {
      setReady(false);
      setErrorLoading(true);
    };
    document.head.appendChild(element);
    return () => {
      urlCache.delete(url);
      document.head.removeChild(element);
    };
  }, [url]);
  return {
    errorLoading,
    ready,
  };
};
const componentCache = new Map();